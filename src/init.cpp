#include"init.h"

namespace init {
	int screenWidth = 1280;
	int screenHeight = 720;

	void init() {
		InitWindow(screenWidth, screenHeight, "ARKANOID");
		SetTargetFPS(60);
	}
}