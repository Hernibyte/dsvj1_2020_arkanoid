#include"menu.h"

namespace menu {

	//----------------------------
	void options(int& caseOption) {
		if (IsKeyPressed(KEY_ENTER)) caseOption = 1;
	}
	void controls() {

	}

	void menu(int& caseOption, int& menuOption, int& gameOption, bool& iwn) {
		menuMovement(menuOption);
		if (IsKeyPressed(KEY_ENTER)) {
			if (menuOption == 1) {
				gameOption = 1;
				caseOption = 2;
			}
			if (menuOption == 2) {
				gameOption = 2;
				caseOption = 2;
			}
			if (menuOption == 3) {
				caseOption = 3;
			}
			if (menuOption == 4) iwn = false;
		}
	}

	//---------------------------

	void menuMovement(int& menuOption) {
		if (IsKeyPressed(KEY_DOWN) && menuOption < 4) menuOption++;
		if (IsKeyPressed(KEY_UP) && menuOption > 1) menuOption--;
	}

	//---------------------------

	void menuDraw(int menuOption) {
		DrawText("ARKANOID", 400, 50, 80, RED);
		DrawText("1 PLAYER", 430, 200, 35, RED);
		DrawText("2 PLAYER", 430, 250, 35, RED);
		DrawText("OPTIONS", 430, 300, 35, RED);
		DrawText("EXIT", 430, 400, 35, RED);
		switch (menuOption)	{
		case 1:
			DrawLine(430, 235, 650, 235, RED);
			break;
		case 2:
			DrawLine(430, 285, 650, 285, RED);
			break;
		case 3:
			DrawLine(430, 335, 650, 335, RED);
			break;
		case 4:
			DrawLine(430, 435, 650, 435, RED);
			break;
		}
	}
}