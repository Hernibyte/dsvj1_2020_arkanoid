#include"game.h"

namespace game {
	struct pj {
		Rectangle box;
		unsigned short int state;
	};

	struct ball {
		Vector2 ballPosition;
		Vector2 ballSpeed;
		float ballRadius;
	};

	Rectangle Bricks[4][10];
	int caseBricks[4][10];

	int caseOption = 1, menuOption = 1, gameOption = 0;
	bool iwn = true;
	bool pause = false;
	bool win = false;
	bool lose = false;
	int collis = 0;

	void start() {
		init::init();

		srand(time(NULL));

		pj Player;
		Player.box = { GetScreenWidth() / 2.0f - 130.0f, GetScreenHeight() - 25.0f, 150, 20 };
		Player.state = 0;

		ball B1;
		B1.ballPosition = { GetScreenWidth() / 2.0f - 80.0f, GetScreenHeight() - 70.0f };
		B1.ballRadius = 20.0f;
		B1.ballSpeed = { -8.0f, -11.0f };

		setMatrix::set(Bricks, caseBricks);

		while (!WindowShouldClose() && iwn == true) {

			upgrade::upgrade(caseOption, menuOption, gameOption, iwn, 
							 B1.ballPosition, B1.ballSpeed, B1.ballRadius, 
							 Player.box, pause, win, lose, Bricks, caseBricks, collis);

			draw::draw(caseOption, menuOption, gameOption, 
					   B1.ballPosition, B1.ballRadius, Player.box, pause, win, lose,Bricks, caseBricks);
		}
		CloseWindow();
	}
}