#ifndef MENU_H
#define MENU_H

#include"Includes.h"

namespace menu {
	void options(int& caseOption);
	void controls();
	void menu(int& caseOption, int& menuOption, int& gameOption, bool& iwn);
	void menuMovement(int& menuOption);
	void menuDraw(int menuOption);
}

#endif