#include"gameLoop.h"

namespace gameloop {

	void setGame(int& caseOption, int& menuOption, int& gameOption, Vector2& ballPosition,
				 Vector2& ballSpeed, float& ballRadius, Rectangle& box, bool& pause, bool& win, 
				 bool& lose, Rectangle Bricks[4][10], int caseBricks[4][10], int& collis) {
		switch (gameOption) {
		case 1:
			onePlGame(ballPosition, ballSpeed, ballRadius, box, pause, win, caseOption, lose, Bricks, caseBricks, collis);
			break;
		case 2:
			twoPlGame(caseOption);
			break;
		}
	}

	void onePlGame(Vector2& ballPosition, Vector2& ballSpeed, 
				   float& ballRadius, Rectangle& box, bool& pause, bool& win, int& caseOption, 
				   bool& lose, Rectangle Bricks[4][10], int caseBricks[4][10], int& collis) {
		float frameTime;
		bool Collisionbox;
		bool CollisionPj;
		int ver;
		ver = 0;

		if (!pause && !win && !lose) {
			frameTime = GetFrameTime();

			ballPosition.x += ballSpeed.x;
			ballPosition.y += ballSpeed.y;
			if ((ballPosition.x >= (GetScreenWidth() - ballRadius)) || (ballPosition.x <= ballRadius)) ballSpeed.x *= -1.0f; //wall collision
			if ((ballPosition.y >= (GetScreenHeight() - ballRadius)) || (ballPosition.y <= ballRadius)) ballSpeed.y *= -1.0f; //wall collision
			if (IsKeyDown(KEY_A)) box.x -= 1000 * frameTime; //control
			if (IsKeyDown(KEY_D)) box.x += 1000 * frameTime; //control
			if (ballPosition.y >= GetScreenHeight() - ballRadius) lose = true;
			CollisionPj = CheckCollisionCircleRec(ballPosition, ballRadius, box);

			if (CollisionPj && collis == 0) {
				ballSpeed.y *= -1.01f;
				ballSpeed.x *= 1.005f;
				collis = 1;
			}
			if (collis == 1) collis = 0;

			if ((box.x + 150) >= GetScreenWidth()) box.x = GetScreenWidth() - 150.0f;
			else
				if (box.x <= 0) box.x = 0;

			for (int i = 0; i < 4; i++) {
				for (int j = 0; j < 10; j++) {
					if (caseBricks[i][j] == 0) ver++;
					if (caseBricks[i][j] == 1) {
						Collisionbox = CheckCollisionCircleRec(ballPosition, ballRadius, Bricks[i][j]);
						if (Collisionbox && collis == 0) {
							Bricks[i][j] = { NULL, NULL, NULL, NULL };
							caseBricks[i][j] = 0;
							ballSpeed.y *= -1.0f;
							collis = 1;
						}
						if (collis == 1) collis = 0;
					}
					if (caseBricks[i][j] == 2) {
						Collisionbox = CheckCollisionCircleRec(ballPosition, ballRadius, Bricks[i][j]);
						if (Collisionbox && collis == 0) {
							caseBricks[i][j] = 1;
							ballSpeed.y *= -1.0f;
							collis = 1;
						}
						if (collis == 1) collis = 0;
					}
				}
			}

			if (ver == 40) win = true;
			if (IsKeyPressed(KEY_SPACE)) pause = true;
		}
		else 
			if (pause) {
				if (IsKeyPressed(KEY_SPACE)) pause = false;
				if (IsKeyPressed(KEY_ENTER)) {
					box = { GetScreenWidth() / 2.0f - 130.0f, GetScreenHeight() - 25.0f, 150, 20 };
					ballPosition = { GetScreenWidth() / 2.0f - 80.0f, GetScreenHeight() - 70.0f };
					ballSpeed = { -8.0f, -11.0f };
					setMatrix::set(Bricks, caseBricks);
					caseOption = 1;
					pause = false;
				}
			}

		if (win) {
			if (IsKeyPressed(KEY_ENTER)) {
				box = { GetScreenWidth() / 2.0f - 130.0f, GetScreenHeight() - 25.0f, 150, 20 };
				ballPosition = { GetScreenWidth() / 2.0f - 80.0f, GetScreenHeight() - 70.0f };
				ballSpeed = { -8.0f, -11.0f };
				win = false;
				setMatrix::set(Bricks, caseBricks);
			}
			if (IsKeyPressed(KEY_BACKSPACE)) {
				box = { GetScreenWidth() / 2.0f - 130.0f, GetScreenHeight() - 25.0f, 150, 20 };
				ballPosition = { GetScreenWidth() / 2.0f - 80.0f, GetScreenHeight() - 70.0f };
				ballSpeed = { -8.0f, -11.0f };
				win = false;
				caseOption = 1;
				setMatrix::set(Bricks, caseBricks);
			}
		}
		if (lose) {
			if (IsKeyPressed(KEY_ENTER)) {
				box = { GetScreenWidth() / 2.0f - 130.0f, GetScreenHeight() - 25.0f, 150, 20 };
				ballPosition = { GetScreenWidth() / 2.0f - 80.0f, GetScreenHeight() - 70.0f };
				ballSpeed = { -8.0f, -11.0f };
				caseOption = 1;
				lose = false;
				setMatrix::set(Bricks, caseBricks);
			}
		}
	}

	void twoPlGame(int& caseOption) {
		if (IsKeyPressed(KEY_ENTER)) caseOption = 1;;
	}

	void drawGame(int gameOption, Vector2 ballPosition, float ballRadius, 
				  bool& pause, bool& win, bool& lose,
				  Rectangle box, Rectangle Bricks[4][10], int caseBricks[4][10]) {
		switch (gameOption) {
		case 1:
			drawOnePlGame(ballPosition, ballRadius, box, pause, win, lose, Bricks, caseBricks);
			break;
		case 2:
			drawTwoPlGame();
			break;
		}
	}

	void drawOnePlGame(Vector2 ballPosition, float ballRadius, Rectangle box, 
					   bool& pause, bool& win, bool& lose,
					   Rectangle Bricks[4][10], int caseBricks[4][10]) {
		if (!pause && !win && !lose) {
			DrawRectangleRec(box, MAROON);
			DrawCircle(ballPosition.x, ballPosition.y, ballRadius, GOLD);

			for (int i = 0; i < 4; i++) {
				for (int j = 0; j < 11; j++) {
					if (caseBricks[i][j] == 1) DrawRectangleRec(Bricks[i][j], RED);
					if (caseBricks[i][j] == 2) DrawRectangleRec(Bricks[i][j], BLUE);
				}
			}
		}
		else 
			if (pause) {
				DrawText("PAUSE", GetScreenWidth() / 2 - 130, GetScreenHeight() / 2 - 250, 75, BLACK);
				DrawText("PRESS SPACE TO RESUME", 100, GetScreenHeight() - 150, 50, BLACK);
				DrawText("PRESS ENTER BACK TO MENU", 100, GetScreenHeight() - 80, 50, BLACK);
			}

		if (win) {
			DrawText("YOU WIN THIS LEVEL", GetScreenWidth() / 2 - 350, GetScreenHeight() / 2 - 250, 70, BLACK);
			DrawText("PRESS ENTER TO CONTINUE", 100, GetScreenHeight() - 150, 50, BLACK);
			DrawText("PRESS BACKSPACE BACK TO MENU", 100, GetScreenHeight() - 80, 50, BLACK);
		}
		if (lose) {
			DrawText("YOU LOSE THIS LEVEL", GetScreenWidth() / 2 - 350, GetScreenHeight() / 2 - 250, 70, BLACK);
			DrawText("PRESS ENTER TO CONTINUE", 100, GetScreenHeight() - 150, 50, BLACK);
		}
	}

	void drawTwoPlGame() {
		DrawText("WORK IN PROGRESS", GetScreenWidth() / 2 - 350, GetScreenHeight() / 2 - 250, 70, BLACK);
		DrawText("PRESS ENTER TO CONTINUE", 100, GetScreenHeight() - 150, 50, BLACK);
	}

	void drawControls() {
		DrawText("CONTROLS", GetScreenWidth() / 2 - 200, GetScreenHeight() / 2 - 250, 70, BLACK);
		DrawText("PRESS A TO MOVE LEFT", 300, GetScreenHeight() - 420, 50, BLACK);
		DrawText("PRESS D TO MOVE RIGHT", 300, GetScreenHeight() - 320, 50, BLACK);
		DrawText("PRESS ENTER TO CONTINUE", 100, GetScreenHeight() - 80, 35, BLACK);
	}
}