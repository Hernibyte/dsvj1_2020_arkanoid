#ifndef UPGRADE_H
#define UPGRADE_H

#include"menu.h"
#include"gameLoop.h"

namespace upgrade {
	void upgrade(int& caseOption, int& menuOption, int& gameOption, bool& iwn,
				 Vector2& ballPosition, Vector2& ballSpeed, float& ballRadius,
				 Rectangle& box, bool& pause, bool& win, bool& lose, Rectangle Bricks[4][10], 
				 int caseBricks[4][10], int& collis);
}

#endif // !UPGRADE_H
