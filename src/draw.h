#ifndef DRAW_H
#define DRAW_H

#include"menu.h"
#include"gameLoop.h"

namespace draw {
	void draw(int caseOption, int menuOption, int gameOption,
			  Vector2 ballPosition, float ballRadius, Rectangle box, 
			  bool& pause, bool& win, bool& lose,
			  Rectangle Bricks[4][10], int caseBricks[4][10]);
}

#endif // !DRAW_H