#include"draw.h"

namespace draw {
	void draw(int caseOption, int menuOption, int gameOption, 
			  Vector2 ballPosition, float ballRadius, Rectangle box, 
			  bool& pause, bool& win, bool& lose,
			  Rectangle Bricks[4][10], int caseBricks[4][10]) {
		BeginDrawing();
		ClearBackground(RAYWHITE);
		//
		switch (caseOption){
		case 1:
			menu::menuDraw(menuOption);
			break;
		case 2:
			gameloop::drawGame(gameOption, ballPosition, ballRadius, 
							   pause, win, lose, box,
							   Bricks, caseBricks);
			break;
		case 3:
			gameloop::drawControls();
			break;
		}
		//
		EndDrawing();
	}
}