#ifndef GAMELOOP_H
#define GAMELOOP_H

#include"Includes.h"
#include"setMatrix.h"

namespace gameloop {
	void setGame(int& caseOption, int& menuOption, int& gameOption, Vector2& ballPosition, 
				 Vector2& ballSpeed, float& ballRadius, Rectangle& box, bool& pause, bool& win, 
				 bool& lose, Rectangle Bricks[4][10], int caseBricks[4][10], int& collis);
	void onePlGame(Vector2& ballPosition, Vector2& ballSpeed, 
				   float& ballRadius, Rectangle& box, 
				   bool& pause, bool& win, int& caseOption, bool& lose,
				   Rectangle Bricks[4][10], 
				   int caseBricks[4][10], int& collis);
	void twoPlGame(int& caseOption);
	void drawGame(int gameOption, Vector2 ballPosition,
				  float ballRadius, bool& pause,
				  bool& win, bool& lose,
				  Rectangle box, 
				  Rectangle Bricks[4][10], int caseBricks[4][10]);
	void drawOnePlGame(Vector2 ballPosition, float ballRadius,
					   Rectangle box, bool& pause,
					   bool& win, bool& lose,
					   Rectangle Bricks[4][10], 
					   int caseBricks[4][10]);
	void drawTwoPlGame();
	void drawControls();
}

#endif // GAMELOOP_H