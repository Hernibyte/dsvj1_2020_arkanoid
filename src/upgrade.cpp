#include"upgrade.h"

namespace upgrade {

	void upgrade(int& caseOption, int& menuOption, int& gameOption, bool& iwn, 
				 Vector2& ballPosition, Vector2& ballSpeed, float& ballRadius, 
				 Rectangle& box, bool& pause, bool& win, bool& lose, 
				 Rectangle Bricks[4][10], int caseBricks[4][10], int& collis) {
		//
		switch (caseOption) {
		case 1:
			menu::menu(caseOption, menuOption, gameOption, iwn);
			break;
		case 2:
			gameloop::setGame(caseOption, menuOption, gameOption, 
							  ballPosition, ballSpeed, ballRadius, 
							  box, pause, win, lose, Bricks, caseBricks, collis);
			break;
		case 3:
			menu::options(caseOption);
			break;
		}
		//
	}
}