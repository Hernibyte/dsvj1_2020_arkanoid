#ifndef GAME_H
#define GAME_H

#include"Includes.h"
#include"init.h"
#include"draw.h"
#include"upgrade.h"
#include"setMatrix.h"

namespace game {
	void start();
}

#endif 