#include"setMatrix.h"

namespace setMatrix {

	void set(Rectangle Bricks[4][10], int caseBricks[4][10]) {
		float brick1Pos = 100.0f;
		float brick2Pos = 100.0f;
		float brick3Pos = 100.0f;
		float brick4Pos = 100.0f;
		float brick5Pos = 100.0f;
		float brick6Pos = 100.0f;

		for (int i = 0; i < 6; i++) {
			for (int j = 0; j < 10; j++) {
				caseBricks[i][j] = rand() % 3;
				if (caseBricks[i][j] == 0) {
					Bricks[i][j] = { NULL, NULL, NULL, NULL };
				}
				if (caseBricks[i][j] == 1) {
					switch (i) {
					case 0:
						Bricks[i][j] = { brick1Pos, 50.0f, 100, 20 };
						brick1Pos += 105.0f;
						break;
					case 1:
						Bricks[i][j] = { brick2Pos, 95.0f, 100, 20 };
						brick2Pos += 105.0f;
						break;
					case 2:
						Bricks[i][j] = { brick3Pos, 140.0f, 100, 20 };
						brick3Pos += 105.0f;
						break;
					case 3:
						Bricks[i][j] = { brick4Pos, 185.0f, 100, 20 };
						brick4Pos += 105.0f;
						break;
					}
				}
				if (caseBricks[i][j] == 2) {
					switch (i) {
					case 0:
						Bricks[i][j] = { brick1Pos, 50.0f, 100, 20 };
						brick1Pos += 105.0f;
						break;
					case 1:
						Bricks[i][j] = { brick2Pos, 95.0f, 100, 20 };
						brick2Pos += 105.0f;
						break;
					case 2:
						Bricks[i][j] = { brick3Pos, 140.0f, 100, 20 };
						brick3Pos += 105.0f;
						break;
					case 3:
						Bricks[i][j] = { brick4Pos, 185.0f, 100, 20 };
						brick4Pos += 105.0f;
						break;
					}
				}
			}
		}
	}
}